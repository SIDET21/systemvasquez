import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Import Containers
import { DefaultLayoutComponent } from './containers';
import { LayoutComponent } from './layout/layout.component';

import { IsAuthenticated } from './utils/auth-guards';
import { P404Component } from './views/error/404.component';
import { P500Component } from './views/error/500.component';
import { LoginComponent } from './views/login/login.component';
import { RegisterComponent } from './views/register/register.component';

export const routes: Routes = [
  // {
  //   path: '',
  //   redirectTo: 'inicio/home',
  //   pathMatch: 'full',
  // },
  // {
  //   path: '',
  //   redirectTo: 'dashboard',
  //   pathMatch: 'full',
  // },
  {
    path: '404',
    component: P404Component,
    data: {
      title: 'Page 404'
    }
  },
  {
    path: '500',
    component: P500Component,
    data: {
      title: 'Page 500'
    }
  },
  {
    path: 'login',
    component: LoginComponent,
    data: {
      title: 'Login Page'
    }
  },
  {
    path: 'register',
    component: RegisterComponent,
    data: {
      title: 'Register Page'
    }
  },
  // {
  //   path: 'Admin',canActivate: [IsAuthenticated],
  //   component: DefaultLayoutComponent,
  //   data: {
  //     title: 'Inicio'
  //   },
  //   children: [

  //     {
  //       path: 'dashboard',
  //       loadChildren: () => import('./views/dashboard/dashboard.module').then(m => m.DashboardModule)
  //     },
  //     {
  //       path: 'productos',
  //       loadChildren: () => import('./views/sistema-gestion/productos/productos.module').then(m => m.ProductosModule)
  //     },
  //     {
  //       path: 'categorias',
  //       loadChildren: () => import('./views/sistema-gestion/categorias/categorias.module').then(m => m.CategoriasModule)
  //     },
  //     {
  //       path: 'ventas',
  //       loadChildren: () => import('./views/sistema-gestion/ventas/ventas.module').then(m => m.VentasModule)
  //     },
  //     {
  //       path: 'marcas',
  //       loadChildren: () => import('./views/sistema-gestion/marca/marca.module').then(m => m.MarcaModule)
  //     },
  //     {
  //       path: 'configuracion',
  //       loadChildren: () => import('./views/sistema-gestion/configuracion/configuracion.module').then(m => m.ConfiguracionModule)
  //     },
  //   ]
  // },
  {
    path: 'admin',
    canActivate: [IsAuthenticated],
        data: {
      title: 'Inicio'
    },
    loadChildren: () => import('./views/sistema-gestion/sistema-gestion.module').then(m => m.SistemaGestionModule)
  },
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: '',
        redirectTo: '/home',
        pathMatch: 'full',
      },
      {
        path: 'home',
        loadChildren: () => import('./views/home/home.module').then(m => m.HomeModule)
      },
      {
        path: 'products',
        loadChildren: () => import('./views/productos/productos.module').then(m => m.ProductosModule)
      },
      // {
      //   path: 'contact',
      //   loadChildren: () => import('./contact/contact.module').then(m => m.ContactModule)
      // },
      // {
      //   path: 'order',
      //   loadChildren: () => import('./order/order.module').then(m => m.OrderModule)
      // },
      // {
      //   path: 'demo',
      //   loadChildren: () => import('./demo/demo.module').then(m => m.DemoModule)
      // },
    ]
  },

  // {
  //   path: '',
  //   component: VentasLayaoutComponent,

  //   children :[
  //     {
  //       path: 'inicio',
  //       loadChildren: () => import('./views/sistema-ventas/inicio/inicio.module').then(m => m.InicioModule)
  //     }
  //   ]
  // },
  { path: '**', component: P404Component }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' }) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
