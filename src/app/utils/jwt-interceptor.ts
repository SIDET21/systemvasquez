import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from '../../environments/environment';
import { catchError, map } from 'rxjs/operators';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { SecurityService } from "./security/security.service";


@Injectable()
export class JWTInterceptor implements HttpInterceptor{

   @BlockUI() blockUI: NgBlockUI;
    constructor(private authenticationService :SecurityService){}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const isLoggedIn = this.authenticationService.isAuthenticated();
        const isApiUrl = request.url.startsWith(environment.URL_SERVICES);
        // request = request.clone({
        //     setHeaders: {
        //         'X-Frame-Options': 'SAMEORIGIN',
        //         'X-XSS-Protection': '1; mode=block',
        //         'X-Content-Type-Options': 'nosniff'
        //     }
        // });
        //debugger;
        //console.log("interceptior")
        this.blockUI.start('Cargando...');

        // // // request = request.clone({
        // // //     setHeaders: {
        // // //         Authorization: `Bearer ${this.authenticationService.getAutorizationToken()}`,
        // // //         'X-Frame-Options': 'sameorigin'
        // // //     }
        // // // });

        if (isLoggedIn && isApiUrl) {
            request = request.clone({
                setHeaders: {
                    Authorization: `Bearer ${this.authenticationService.getAuthorizationToken()}`
                    //, 'X-Frame-Options': 'sameorigin'
                }
            });
        }
        return next.handle(request)
        .pipe(
            map((event: HttpEvent<any>) => {
      
                if (event instanceof HttpResponse) {
                    if (request.method == "POST"||  request.method == "PUT") {
                      this.blockUI.stop();
                      return event.clone();
                     
                    } else if(request.method == "GET"){
                      this.blockUI.stop();
                      return event;
                    }else{
                      return event;
                    }
                   
                  }
             // return event;
            }
            )
          );    
    }




    }