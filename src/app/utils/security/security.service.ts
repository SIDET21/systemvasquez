import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import {JwtHelperService} from '@auth0/angular-jwt';
import { Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { ApiResponseModel } from '../../models/api-response-model';
import { UserLogin, UserSesionData } from '../../models/user/user';

const API_URL = environment.URL_SERVICES;
// const httpOptions = {
//   headers: new HttpHeaders({
//     'Content-Type': 'application/json',
//   })
// };
@Injectable({
  providedIn: 'root'
})
export class SecurityService {

  
  private session = new Subject<boolean>();
  jwtHelper: JwtHelperService = new JwtHelperService();
  constructor(private http: HttpClient, private router: Router) { }



  // GetLogin(user: UserLogin): Observable<ApiResponseModel<UserSesionData>> {
  //   return this.http.post<ApiResponseModel<UserSesionData>>(API_URL + "Auth/Login", user, httpOptions).pipe(map(res => res));
  // }
  GetLogin(user: UserLogin): Observable<ApiResponseModel<any>> {
    return this.http.get<ApiResponseModel<any>>('assets/data/datatoken.json');
  }

  // getProducts(): Observable<ApiResponseModel<producto[]>> {
  //   return this.http.get<ApiResponseModel<producto[]>>('assets/data/dataProduct.json');
  // }

  // GetEncrypt(passb64 : string64): Observable<any> {
  //   return this.http.post<any>(API_URL + "Auth/Encrypt", passb64, httpOptions).pipe(map(res => res));
  // }




  getSessionCambio() {
    return this.session.asObservable();
  }

  verificarSesion = async () => {
    const helper = new JwtHelperService();
    const decodedToken = helper.decodeToken(sessionStorage.getItem('token'));
    if (decodedToken !== undefined && decodedToken !== null) {
      return true;
    } else {
      return false;
    }
  }

  isAuthenticated(): boolean {
    const token = sessionStorage.getItem('accessToken');
    if (token === null) {
        return false;
    }
    return this.jwtHelper.isTokenExpired(token) === false;
  }

  setSessionCambio(status: boolean) {
    this.session.next(status);
  }

  getAuthorizationToken(): string {
    return sessionStorage.getItem('accessToken');
  }

  resetSecurityObject(): void {
    sessionStorage.removeItem('accessToken');
  }

  cerrarSesion() {
    this.setSessionCambio(false);
    sessionStorage.clear();
    console.log('Se borro tokens de storage');
    setTimeout(() => {
      this.router.navigate(['/login']);
    }, 500);
  }



  getUserName(): string {
    const token = sessionStorage.getItem('accessToken');
    if (token == null) {
        return '';
    }
    const decodedToken = this.jwtHelper.decodeToken(token);
    return decodedToken.name;
  }

  getUserLastName(): string {
    const token = sessionStorage.getItem('accessToken');
    if (token == null) {
        return '';
    }
    const decodedToken = this.jwtHelper.decodeToken(token);
    return decodedToken.lastname;
  }

  getUserRol(): string {
    const token = sessionStorage.getItem('accessToken');
    if (token == null) {
        return '';
    }
    const decodedToken = this.jwtHelper.decodeToken(token);
    return decodedToken.role;
  }


  getUserEntity(): string {
    const token = sessionStorage.getItem('accessToken');
    if (token == null) {
        return '';
    }
    const decodedToken = this.jwtHelper.decodeToken(token);
    return decodedToken.entity;
  }


  getUserCod(): string {
    const token = sessionStorage.getItem('accessToken');
    if (token == null) {
        return '';
    }
    const decodedToken = this.jwtHelper.decodeToken(token);
    return decodedToken.code;
  }
}
