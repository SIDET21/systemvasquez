import { Router } from "@angular/router";
import { Injectable } from "@angular/core";
import Swal from "sweetalert2";
import { BlockUI, NgBlockUI } from "ng-block-ui";
import { AngularFireStorage  } from "@angular/fire/storage";
import { fromEvent, Observable, of } from "rxjs";
import { finalize, pluck } from "rxjs/operators";
import { AngularFireDatabase, AngularFireList } from "@angular/fire/database";
//import { getStorage, ref, getDownloadURL } from "firebase/storage";
//import { FileUpload } from "../models/producto/producto";
import {
  AngularFirestore,
  AngularFirestoreCollection,
  DocumentReference,
} from "@angular/fire/firestore";
import { filenamePath } from "./constans";
import { DataFileUpload } from "../models/producto/producto";
//import { NgxSpinnerService } from 'ngx-spinner';

@Injectable({
  providedIn: "root",
})
export class FuncionesService {
  @BlockUI() blockUI: NgBlockUI;
  basePath = "Productos";
  imageDetailList: AngularFireList<any>;
  fileList: any[];
  msg: string = "error";
  constructor(
    private router: Router,
    private storage: AngularFireStorage,
    private db: AngularFireDatabase, //,private spinner: NgxSpinnerService
    private afs: AngularFirestore // private Collection: AngularFirestoreCollection
  ) {}

  mostrarCargando(subtitle: string = null, title: string = null) {
    // this.spinner.show();
    // return Swal.showLoading();
    Swal.fire({
      icon: "info",
      title: title,
      html: subtitle,
      // timer: 2000,
      showConfirmButton: false,
      allowOutsideClick: false,
      timerProgressBar: true,
      didOpen: () => {
        Swal.showLoading();
      },
    });
  }

  ocultarCargando() {
    // this.spinner.hide();
    // return this;
    return Swal.close();
  }

  mensajeOk(text: string, accionRedirect?: string): Promise<null> {
    return new Promise((resolve) => {
      Swal.fire({
        text: text,
        icon: "success",
        confirmButtonText: "Aceptar",
        allowOutsideClick: false,
        confirmButtonColor: "green",
      }).then(() => {
        if (accionRedirect !== undefined) {
          this.router.routeReuseStrategy.shouldReuseRoute = () => false;
          this.router.navigate([accionRedirect]);
        }
      });
    });
  }

  mensajeInfo(text: string, accionRedirect?: string): Promise<null> {
    return new Promise((resolve) => {
      Swal.fire({
        text: text,
        icon: "info",
        confirmButtonText: "Aceptar",
        allowOutsideClick: false,
        confirmButtonColor: "#3085d6",
      }).then(() => {
        if (accionRedirect !== undefined) {
          this.router.routeReuseStrategy.shouldReuseRoute = () => false;
          this.router.navigate([accionRedirect]);
        }
      });
    });
  }

  mensajeError(text: string): Promise<void> {
    return new Promise((resolve, reject) => {
      Swal.fire({
        text: text,
        icon: "error",
        confirmButtonText: "Aceptar",
        allowOutsideClick: false,
        confirmButtonColor: "#23DF05",
      }).then(() => {
        resolve();
      });
    });
  }

  mensajeConfirmar(text: string, title: string = null) {
    return new Promise<any>((resolve, reject) => {
      Swal.fire({
        title: title,
        text: text,
        icon: "question",
        allowOutsideClick: false,
        allowEscapeKey: false,
        showCancelButton: true,
        cancelButtonColor: "#b5b3b3",
        confirmButtonColor: "#1BADC3",
        cancelButtonText: "Cancelar",
        confirmButtonText: "Aceptar",
        reverseButtons: false,
      }).then(
        (result) => {
          resolve(result.value);
        },
        (msg) => {
          reject(msg);
        }
      );
    });
  }

  changueName(DataFile) {
    const files = DataFile;
    var resultListFiles = [];
    for (let i = 0; i < files.length; i++) {
      let fileToUpload = <File>files[i];
      let fileName = String(i + 1);
      let fileExtension = fileToUpload.name.split("?")[0].split(".").pop();
      const formData = new FormData();
      formData.append("file", fileToUpload, fileName + "." + fileExtension);
      let data = formData.get("file");
      resultListFiles.push(data);
    }

    return resultListFiles;
  }

  async subirArchivo(filePathPrincipal, FilepathID, files) {
    let listURL = [];
    //let fileUpload = new FileUpload(files[0])
    this.blockUI.start("Cargando...");
    const dataFiles = this.changueName(files);
    const filepath = `${filePathPrincipal}/${FilepathID}/`;

    for (let i = 0; i < dataFiles.length; i++) {
      try {
        var storageRef = this.storage.ref(filepath + dataFiles[i].name);
        var task = this.storage.upload(
          filepath + dataFiles[i].name,
          dataFiles[i]
        );

        task
          .snapshotChanges()
          .pipe(
            finalize(() => {
              storageRef.getDownloadURL().subscribe((urlImage) => {
                listURL.push(String(urlImage));
               // console.log("URLIMAGE", urlImage);
              });
            })
          )
          .subscribe();
      } catch (error) {
        console.log("error ", error);
      }
    }
    console.log("ListURL", listURL);

    var resultado = task.percentageChanges().toPromise();

    return resultado;
  }

  getFileUploads(numberItems, path): AngularFireList<any> {
    return this.db.list(path, (ref) => ref.limitToLast(numberItems));
  }

  getImages() {
    return this.afs.collection("Productos").snapshotChanges();
  }

  uploadDataImage(collection, dataProduct) {
    const path = collection;
    return this.afs
      .collection<DataFileUpload>(path)
      .doc(dataProduct.id)
      .set(dataProduct);
  }

  getBase64(event) {
    //let me = this;
    let file = event.target.files[0];
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function () {
      //me.modelvalue = reader.result;
      console.log(reader.result);
    };
    reader.onerror = function (error) {
      console.log("Error: ", error);
    };
  }

  imageToBase64(
    fileReader: FileReader,
    fileToRead: File[]
  ): Observable<string> {
    let reader = new FileReader();
    for (let i = 0; i < fileToRead.length; i++) {
      reader.readAsDataURL(fileToRead[i]);
    }

    return fromEvent(reader, "load").pipe(pluck("currentTarget", "result"));
  }


}
