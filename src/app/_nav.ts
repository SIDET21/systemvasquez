import { INavData } from '@coreui/angular';

export const navItems: INavData[] = [
  {
    name: 'Dashboard',
    url: '/admin/dashboard',
    icon: 'icon-speedometer',

  },
  {
    divider: true
  },

  
  {    
    name: "mantenimiento",
    //url: "/buttons",
    icon: "icon-cursor",
    
    children: [
      {
        name: "Productos",
        url: "/admin/productos",
        icon: "cil-layers",
      },
      {
        name: "Categorías",
        url: "/admin/categorias",
        icon: "cil-library",
      },
      {
        name: "Marcas",
        url: "/admin/marcas",
        icon: "cil-library",
      },
    ],
  }, 






  {
    divider: true
  },
  {
    title: true,
    name: 'Ventas',
  },
  {
    name: 'Lista de ventas',
    url: '/admin/ventas',
    icon: 'cil-cart',

  },
  {
    divider: true
  },
  {
    title: true,
    name: 'Configuración',
  },
  {
    name: 'Banner',
    url: '/configuracion/banner',
    icon: 'cil-cart',

  }
  
];
