import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { ApiResponseModel } from "../../models/api-response-model";
import { UserLogin, UserSesionData } from "../../models/user/user";
import { SecurityService } from "../../utils/security/security.service";

@Component({
  selector: "app-dashboard",
  templateUrl: "login.component.html",
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  /**Formularios */
  Formulario: FormGroup;
  /** User */
  RequestUser: UserLogin = new UserLogin();
  userSesionData: ApiResponseModel<UserSesionData> =
    new ApiResponseModel<UserSesionData>();
  mensaje: string = "";
  isLoading: boolean;
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private securityService: SecurityService
  ) {}

  ngOnInit(): void {
    this.initForm();
  }

  initForm() {
    this.Formulario = this.fb.group({
      fm_User: this.fb.control("", [Validators.required]),
      fm_Pass: this.fb.control("", [Validators.required]),
      //recaptchaReactive: this.fb.control(""),
    });
  }

  login() {
    this.isLoading = true;
    if (this.Formulario.invalid) {
      return (this.mensaje = "Formulario inválido");
    }

    this.RequestUser.user = this.Formulario.controls["fm_User"].value;
    this.RequestUser.password = this.Formulario.controls["fm_Pass"].value;

    sessionStorage.setItem("accessToken", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjb2RpZ28iOiIxMjM0NTY3ODkwIiwibm9tYnJlIjoiSm9obiBEb2UiLCJyb2wiOiJBRE1JTklTVFJBRE9SIiwiaWF0IjoxNTE2MjM5MDIyfQ.jQYl0iw2q015jyA52c71M_BK4GvZgSbAzgckXsAQy9I");
    this.router.navigate(["admin/dashboard"]);


    // this.securityService.GetLogin(this.RequestUser).subscribe(
    //   (data) => {
    //     this.isLoading = false;
    //     this.userSesionData = data;
    //     if (
    //       this.userSesionData.success &&
    //       this.userSesionData.data.token !== " "
    //     ) {
    //       sessionStorage.setItem("accessToken", this.userSesionData.data.token);
    //       this.router.navigate(["dashboard"]);
    //     } //else
    //     if (this.userSesionData.data.token === "") {
    //       // this.RequerdCaptcha =true;
    //       this.mensaje = "Usuario y/o contraseña incorrecto"; //data.error.message;
    //       // this.intent++;
    //       // if (this.intent >= MAXINTENT) {
    //       //   this.RequerdCaptcha = true;
    //       // }
    //     }
    //   },
    //   (error) => {
    //     this.isLoading = false;
    //     console.log("Error en el servicio: ", error);
    //     this.mensaje = "Error de servicio , intente de nuevo o mas tarde.";
    //   }
    // );
  }

  formInvalid(control: string) {
    return (
      this.Formulario.get(control).invalid &&
      (this.Formulario.get(control).dirty ||
        this.Formulario.get(control).touched)
    );
  }
}
