import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DefaultLayoutComponent } from '../../containers';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { ListaCategoriasComponent } from './categorias/lista-categorias/lista-categorias.component';
import { ListaMarcaComponent } from './marca/lista-marca/lista-marca.component';
import { ListaProductosComponent } from './productos/lista-productos/lista-productos.component';
import { ListaVentasComponent } from './ventas/lista-ventas/lista-ventas.component';

const routes: Routes = [
  {
    path: '',
    component: DefaultLayoutComponent,
    data: {
      title: 'admin'
    },
    children: [
      {
        path: '',
        component: DashboardComponent,
        data: {
          title: 'Dashboard'
        }
      },
      {
        path: 'dashboard',
        component: DashboardComponent,
        data: {
          title: 'Dashboard'
        }
      },
      {
        path: 'productos',
        component: ListaProductosComponent,
         data: { title: 'lista de productos' },pathMatch :'full'
      },
      {
        path: 'ventas',
        component: ListaVentasComponent,
        data: { title: 'lista de ventas' },pathMatch :'full'
      },
      {
        path: 'marcas',
        component: ListaMarcaComponent,
        data: { title: 'lista de marcas' },pathMatch :'full'
      },
      {
        path: 'categorias',
        component: ListaCategoriasComponent,
        data: { title: 'lista de categorías' },pathMatch :'full'
      }

      // {
      //   path: 'table',
      //   component: TableComponent
      // },
      // {
      //   path: 'products',
      //   component: ProductsListComponent
      // },
      // {
      //   path: 'products/create',
      //   component: FormProductComponent
      // },
      // {
      //   path: 'products/edit/:id',
      //   component: ProductEditComponent
      // },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SistemaGestionRoutingModule { }
