import {
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
} from "@angular/core";
import { AngularFireStorage } from "@angular/fire/storage";
import { FormArray, FormBuilder, FormGroup, Validators } from "@angular/forms";
import { BlockUI, NgBlockUI } from "ng-block-ui";
import { ModalDirective } from "ngx-bootstrap/modal";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { producto } from "../../../../models/producto/producto";
import { ProductoService } from "../../../../services/producto/producto.service";
import {
  filenamePath,
  MAXFILESLIMIT,
  MENSAJES,
  type_Action,
} from "../../../../utils/constans";
import { FuncionesService } from "../../../../utils/funciones.service";
import {
  AngularFirestore,
  AngularFirestoreCollection,
} from "@angular/fire/firestore";
import { AngularEditorConfig } from "@kolkov/angular-editor";
//import { Editor } from "ngx-editor";
@Component({
  selector: "app-gestion-producto",
  templateUrl: "./gestion-producto.component.html",
  styleUrls: ["./gestion-producto.component.scss"],
})
export class GestionProductoComponent implements OnInit //,OnDestroy 
{
  /**Formularios */
  Formulario: FormGroup;
  tittle: string = "";
  idProducto = 0;
  uploadedFiles: Array<File> = [];
  producto?: producto = new producto();
  @ViewChild("largeModal") public largeModal: ModalDirective;
  @Input() show: string = type_Action.New;
  @Input() data?: producto = new producto();
  @Output() dataEvent = new EventEmitter<any>();
  files: File[] = [];
  public animation: boolean = false;
  public multiple: boolean = false;
 // editor: Editor;
  html: '';
  mensaje: string = MENSAJES.IMAGEN_PRINCIPAL;

  maxFileslimit = MAXFILESLIMIT;
  uploadProgress: Observable<number>;

  uploadURL: Observable<string>;

  public finalizado = false;
  URLPublica;
  public porcentaje = 0;
  private itemsCollection: AngularFirestoreCollection<any>;
  items: Observable<any[]>;
  @BlockUI() blockUI: NgBlockUI;
  constructor(
    private fb: FormBuilder,
    private funcionesService: FuncionesService,
    private service: ProductoService,
    private storage: AngularFireStorage,
    private afs: AngularFirestore
  ) {
    this.itemsCollection = afs.collection<any>("Productos");
  }

  ngOnInit(): void {
    // this.getData();
    this.initForm();
    this.getDataImages();
    //this.view();
   // this.editor = new Editor();
  }

  // ngOnDestroy(): void {
  //   this.editor.destroy();
  // }

  initForm() {
    this.Formulario = this.fb.group({
      fm_nombre: this.fb.control("", [Validators.required]),
      fm_descripcion: this.fb.control("", [Validators.required]),
      fm_precio: this.fb.control("", [Validators.required]),
      fm_descuento: this.fb.control("", [Validators.required]),
      fm_stock: this.fb.control("", [Validators.required]),
      fm_marca: this.fb.control("", [Validators.required]),
      fm_subcategoria: this.fb.control("", [Validators.required]),
      files: this.fb.array([], [Validators.required]),
      htmlContent :this.fb.control("", [Validators.required])
    });
  }

  modalShow() {
    switch (this.show) {
      case type_Action.New:
        this.tittle = "Nuevo producto";

        break;
      case type_Action.Edit:
        this.tittle = "Modificar producto";
        this.viewData();

        break;
      case type_Action.View:
        this.Formulario.disable();
        this.viewData();
        this.tittle = "Ver producto";

        break;
    }

    this.largeModal.show();
    this.largeModal.config.backdrop = "static";
    this.largeModal.config.keyboard = false;
  }

  modalClose() {
    this.largeModal.hide();
    this.Formulario.reset();
    this.files = [];
  }

  viewData() {
    this.producto = this.data;
    this.idProducto = this.producto.producto_id;
    this.Formulario.controls["fm_nombre"].setValue(this.producto.nombre);
    this.Formulario.controls["fm_descripcion"].setValue(
      this.producto.descripcion
    );
    this.Formulario.controls["fm_precio"].setValue(this.producto.precio);
    this.Formulario.controls["fm_descuento"].setValue(this.producto.descuento);
    this.Formulario.controls["fm_stock"].setValue(this.producto.stock);
    this.Formulario.controls["fm_marca"].setValue(this.producto.marca_id);
    this.Formulario.controls["fm_subcategoria"].setValue(
      this.producto.subcategoria_id
    );
  }

  get fileFormArray(): FormArray {
    return this.Formulario.get("files") as FormArray;
  }

  getRequest() {
    console.log("guardar producto", this.producto);

    this.producto.nombre = this.Formulario.controls["fm_nombre"].value;
    this.producto.descripcion =
      this.Formulario.controls["fm_descripcion"].value;
    this.producto.precio = this.Formulario.controls["fm_precio"].value;
    this.producto.descuento = this.Formulario.controls["fm_descuento"].value;
    this.producto.stock = this.Formulario.controls["fm_stock"].value;
    this.producto.marca_id = this.Formulario.controls["fm_marca"].value;
    this.producto.subcategoria_id =
      this.Formulario.controls["fm_subcategoria"].value;
  }

  Save() {
    if (this.Formulario.invalid)
      return this.funcionesService.mensajeError("Formulario inválido");

    //debugger;
    this.funcionesService
      .mensajeConfirmar(
        `¿Está seguro de ${
          this.idProducto === 0 ? "guardar" : "modificar"
        } los datos ingresados?`,
        ` ${this.idProducto === 0 ? "Guardando" : "Modificando"} producto`
      )
      .then((res) => {

        if(res){
          this.getRequest();
          if (this.idProducto == 0) {
            //GUARDAR;
            this.service.insertProducto(this.producto).subscribe(
              (res) => {
                if (res.success) {
                  this.blockUI.start("Cargando...");
                  const Idproduct = res.data.idproducto;
  
                  //this.uploadData(this.files, Idproduct);
                  this.funcionesService.subirArchivo("Productos",Idproduct,this.files)
                } else {
                  this.funcionesService.mensajeError(res.message);
                }
              },
              (err) => {
                this.funcionesService.mensajeError("Problemas del servicio.");
                console.log("Error en el servicio: ", err);
              }
            );
          } else {
            //MODIFICAR;
            this.service.editProducto(this.producto).subscribe(
              (res) => {
                if (res.success) {
                  this.funcionesService.mensajeOk(
                    "Producto modificado correctamente."
                  );
                  this.modalClose();
                  this.dataEvent.emit(true);
                } else {
                  this.funcionesService.mensajeError(
                    "Error al modificar el producto."
                  );
                }
              },
              (err) => {
                this.funcionesService.mensajeError("Problemas del servicio.");
                console.log("Error en el servicio: ", err);
              }
            );
          }
        }

   
      });
  }

  onSelect(event) {
    if (
      event.addedFiles.length > this.maxFileslimit ||
      this.files.length >= this.maxFileslimit
    )
      return this.funcionesService.mensajeError(
        "Solo se puede subir " + this.maxFileslimit + " imágenes"
      );

    this.files.push(...event.addedFiles);
    let fg = this.fb.group(String);
    this.fileFormArray.push(fg);
  }

  onRemove(event) {
    console.log(event);
    this.files.splice(this.files.indexOf(event), 1);
  }

  getDataImages() {
    this.funcionesService.getImages().subscribe((data) => {
      const datamap = data.map((e) => {
        return {
          id: e.payload.doc.id,
          listaImagenes: e.payload.doc.data()["listImagenes"],
        };
      });

      console.log("DATA", datamap);
    });
  }

  uploadData(dataFile, idProduct) {
    const producto = filenamePath.PRODUCTOS;
    const dataProduct = {
      id: idProduct,
      listImagenes: [],
    };

    const fileReader = new FileReader();
    this.funcionesService
      .imageToBase64(fileReader, dataFile)
      .subscribe((base64image) => {
        console.log("prueba", base64image);
        dataProduct.listImagenes.push(base64image);
        this.funcionesService
          .uploadDataImage(producto, dataProduct)
          .then((response) => {
            this.blockUI.stop();
            this.funcionesService.mensajeOk("se subió la data");

            this.modalClose();
            this.dataEvent.emit(true);
          })
          .catch((err) => {
            this.funcionesService.mensajeError(err);
          });
      });
  }

  editorConfig: AngularEditorConfig = {
    editable: true,
      spellcheck: true,
      height: 'auto',
      minHeight: '0',
      maxHeight: 'auto',
      width: 'auto',
      minWidth: '0',
      translate: 'yes',
      enableToolbar: true,
      showToolbar: true,
      placeholder: 'Enter text here...',
      defaultParagraphSeparator: '',
      defaultFontName: '',
      defaultFontSize: '',
      fonts: [
        {class: 'arial', name: 'Arial'},
        {class: 'times-new-roman', name: 'Times New Roman'},
        {class: 'calibri', name: 'Calibri'},
        {class: 'comic-sans-ms', name: 'Comic Sans MS'}
      ],
      customClasses: [
      {
        name: 'quote',
        class: 'quote',
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1',
      },
    ]
};
}
