import { Component, OnInit, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
import { ModalDirective } from "ngx-bootstrap/modal";
import { ProductoService } from "../../../../services/producto/producto.service";
import { FuncionesService } from "../../../../utils/funciones.service";
import { producto } from "../../../../models/producto/producto";

@Component({
  selector: "app-lista-productos",
  templateUrl: "./lista-productos.component.html",
  styleUrls: ["./lista-productos.component.scss"],
})
export class ListaProductosComponent implements OnInit {
  @ViewChild("largeModal") public largeModal: ModalDirective;
  listaProducto: producto[] = [];
  //modalShows = 1 ;
  tipoCategoria = [
    { value: "1", name: "tablets" },
    { value: "2", name: "pantallas" },
    { value: "3", name: "mouse" },
    { value: "4", name: "laptops" },
  ];

  constructor(
    private funcionesService: FuncionesService,
    private router: Router,
    private service: ProductoService
  ) {}

  ngOnInit(): void {
    this.listAll();
  }

  buscar() {}

  eliminarProducto() {
    this.funcionesService
      .mensajeConfirmar(`¿Está seguro de eliminar el producto?`)
      .then((res) => {
        if (res) {
          this.funcionesService.mensajeOk(
            "Se eliminó el producto correctamente."
          );
        }
      });
  }

  listAll() {
    this.service.getProducts().subscribe(
      (resp) => {
        if (resp.success) {
          this.listaProducto = resp.data;
        } else {
          return this.funcionesService.mensajeError(resp.message);
        }
      },
      (err) => {
        console.error(err);
        return this.funcionesService.mensajeError("Problemas en el servicio");
      }
    );
  }
}
