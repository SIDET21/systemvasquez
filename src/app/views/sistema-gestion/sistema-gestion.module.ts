import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { SistemaGestionRoutingModule } from "./sistema-gestion-routing.module";
import { DefaultLayoutComponent } from "../../containers";
import { DashboardComponent } from "../dashboard/dashboard.component";
import { AppAsideModule, AppBreadcrumbModule, AppFooterModule, AppHeaderModule, AppSidebarModule } from "@coreui/angular";
import { BlockUIModule } from "ng-block-ui";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { BrowserModule } from "@angular/platform-browser";
import { PerfectScrollbarModule } from "ngx-perfect-scrollbar";
import { BsDropdownModule } from "ngx-bootstrap/dropdown";
import { TabsModule } from "ngx-bootstrap/tabs";
import { ChartsModule } from "ng2-charts";
import { IconModule, IconSetModule } from "@coreui/icons-angular";
import { HttpClientModule } from "@angular/common/http";
import { MatDialogModule } from "@angular/material/dialog";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { ListaProductosComponent } from "./productos/lista-productos/lista-productos.component";
import { ModalModule } from "ngx-bootstrap/modal";
import { FileUploadModule } from "@iplab/ngx-file-upload";
import { PaginationModule } from "ngx-bootstrap/pagination";
import { NgxDropzoneModule } from "ngx-dropzone";
import { GestionProductoComponent } from "./productos/gestion-producto/gestion-producto.component";
import { GestionMarcaComponent } from "./marca/gestion-marca/gestion-marca.component";
import { ListaMarcaComponent } from "./marca/lista-marca/lista-marca.component";
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { GestionCategoriasComponent } from "./categorias/gestion-categorias/gestion-categorias.component";
import { ListaCategoriasComponent } from "./categorias/lista-categorias/lista-categorias.component";
import { ListaVentasComponent } from "./ventas/lista-ventas/lista-ventas.component";
import { GestionVentasComponent } from './ventas/gestion-ventas/gestion-ventas.component';
import { AngularEditorModule } from "@kolkov/angular-editor";
const APP_CONTAINERS = [DefaultLayoutComponent];
//import { NgxEditorModule } from 'ngx-editor';

@NgModule({
  declarations: [...APP_CONTAINERS,
     DashboardComponent,
     ListaProductosComponent,
     GestionProductoComponent,
     ListaMarcaComponent,
     GestionMarcaComponent,
     ListaCategoriasComponent,
     GestionCategoriasComponent,
     ListaVentasComponent,
     GestionVentasComponent],
  imports: [CommonModule,
     SistemaGestionRoutingModule,
    //BrowserModule,
   // BrowserAnimationsModule,
    //AppRoutingModule,
    AppAsideModule,
    AppBreadcrumbModule.forRoot(),
    BlockUIModule.forRoot(),
    AppFooterModule,
    AppHeaderModule,
    AppSidebarModule,
    PerfectScrollbarModule,
    //BsDropdownModule.forRoot(),
    //TabsModule.forRoot(),
    ChartsModule,
    IconModule,
   // IconSetModule.forRoot(),
    HttpClientModule,
    MatDialogModule,
    ReactiveFormsModule,
    FormsModule,
    AngularEditorModule,
    //CommonModule,
    //ProductosRoutingModule,
    ModalModule.forRoot(),
    BsDropdownModule,
   // ReactiveFormsModule,
    //FormsModule,
    FileUploadModule,
    //MatDialogModule,
    PaginationModule,
    NgxDropzoneModule,
    //FormsModule,
   // DashboardRoutingModule,
    //ChartsModule,
    //CommonModule,
    BsDropdownModule,
    ButtonsModule.forRoot(),
    
    
  
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class SistemaGestionModule {}
