import { Component, OnInit } from '@angular/core';
import { marca } from '../../../../models/marca/marca';
import { MarcaService } from '../../../../services/marca/marca.service';
import { FuncionesService } from '../../../../utils/funciones.service';

@Component({
  selector: 'app-lista-marca',
  templateUrl: './lista-marca.component.html',
  styleUrls: ['./lista-marca.component.scss']
})
export class ListaMarcaComponent implements OnInit {
  listaMarca :marca [] = [];
  constructor(private servicio :MarcaService,private funcionesService: FuncionesService) { }

  ngOnInit(): void {
    this.listAll();
  }

  listAll() {
    // this.loading = true;
    this.servicio.getMarca().subscribe(
      (resp) => {
        if (resp.success) {
          console.log(resp);
          this.listaMarca = resp.data;
        } else {
          return this.funcionesService.mensajeError(resp.message);
        }

        //this.loading = false;
      },
      (err) => {
        //this.loading = false;
        console.error(err);
        return this.funcionesService.mensajeError("Problemas en el servicio");
      }
    );
  }

}
