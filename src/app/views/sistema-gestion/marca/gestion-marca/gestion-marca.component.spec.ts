import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionMarcaComponent } from './gestion-marca.component';

describe('GestionMarcaComponent', () => {
  let component: GestionMarcaComponent;
  let fixture: ComponentFixture<GestionMarcaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GestionMarcaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionMarcaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
