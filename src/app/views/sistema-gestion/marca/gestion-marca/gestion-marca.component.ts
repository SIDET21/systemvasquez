import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { marca } from '../../../../models/marca/marca';
import { type_Action } from '../../../../utils/constans';

@Component({
  selector: 'app-gestion-marca',
  templateUrl: './gestion-marca.component.html',
  styleUrls: ['./gestion-marca.component.scss']
})
export class GestionMarcaComponent implements OnInit {
  Formulario: FormGroup;
  tittle: string = "";
  idMarca = 0;
  marca?: marca = new marca();
  @ViewChild("largeModal") public largeModal: ModalDirective;
  @Input() show: string = "nuevo"
  @Input() data ?: marca = new marca();
  @Output() dataEvent = new EventEmitter<any>();
  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.initForm();
  }

  initForm() {
    this.Formulario = this.fb.group({
      fm_descripcion: this.fb.control("", [Validators.required]),
    });
  }
  
  modalShow() {
    switch (this.show) {
      case type_Action.New:
        this.tittle = "Nueva Marca";
        break;
      case type_Action.Edit:
        this.tittle = "Modificar Marca";
        this.viewData();
        
        break;
      case type_Action.View:
        this.Formulario.disable();
        this.viewData();
        this.tittle = "Ver Marca";

        break;
    }

    this.largeModal.show();
    this.largeModal.config.backdrop = "static";
    this.largeModal.config.keyboard = false;
  }

  modalClose() {
    this.largeModal.hide();
  }

  viewData(){
    this.marca = this.data;
    this.idMarca = this.marca.marca_id;
    this.Formulario.controls['fm_descripcion'].setValue(this.marca.descripcion);
    

  }
}
