import { Component, OnInit } from '@angular/core';
import { categoria } from '../../../../models/categoria/categoria';
import { CategoriaService } from '../../../../services/categoria/categoria.service';
import { FuncionesService } from '../../../../utils/funciones.service';

@Component({
  selector: 'app-lista-categorias',
  templateUrl: './lista-categorias.component.html',
  styleUrls: ['./lista-categorias.component.scss']
})
export class ListaCategoriasComponent implements OnInit {


  listaCategoria :categoria [] = [];
  constructor(private service : CategoriaService,private funcionesService: FuncionesService) { }

  ngOnInit(): void {
    this.listAll();
  }


  listAll() {
    // this.loading = true;
    this.service.getCategoria().subscribe(
      (resp) => {
        if (resp.success) {
          console.log(resp);
          this.listaCategoria = resp.data;
        } else {
          return this.funcionesService.mensajeError(resp.message);
        }

        //this.loading = false;
      },
      (err) => {
        //this.loading = false;
        console.error(err);
        return this.funcionesService.mensajeError("Problemas en el servicio");
      }
    );
  }

}
