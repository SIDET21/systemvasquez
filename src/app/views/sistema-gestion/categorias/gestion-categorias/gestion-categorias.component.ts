import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { categoria } from '../../../../models/categoria/categoria';
import { type_Action } from '../../../../utils/constans';

@Component({
  selector: 'app-gestion-categorias',
  templateUrl: './gestion-categorias.component.html',
  styleUrls: ['./gestion-categorias.component.scss']
})
export class GestionCategoriasComponent implements OnInit {
  Formulario: FormGroup;
  tittle: string = "";
  idCategoria = 0;
  categoria?: categoria = new categoria();
  @ViewChild("largeModal") public largeModal: ModalDirective;
  @Input() show: string = "nuevo"
  @Input() data ?: categoria = new categoria();
  @Output() dataEvent = new EventEmitter<any>();
  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.initForm();
  }

  initForm() {
    this.Formulario = this.fb.group({
      fm_descripcion: this.fb.control("", [Validators.required]),
    });
  }

  modalShow() {
    switch (this.show) {
      case type_Action.New:
        this.tittle = "Nueva Categoría";
        break;
      case type_Action.Edit:
        this.tittle = "Modificar Categoría";
        this.viewData();
        
        break;
      case type_Action.View:
        this.Formulario.disable();
        this.viewData();
        this.tittle = "Ver Categoría";

        break;
    }

    this.largeModal.show();
    this.largeModal.config.backdrop = "static";
    this.largeModal.config.keyboard = false;
  }

  modalClose() {
    this.largeModal.hide();
  }

  viewData(){
    this.categoria = this.data;
    this.idCategoria = this.categoria.categoria_id;
    this.Formulario.controls['fm_descripcion'].setValue(this.categoria.descripcion);
    

  }
}
