import { Component, OnInit,ElementRef } from '@angular/core';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { CartService } from '../../../services/cart/cart.service';
@Component({
  selector: 'app-header-home',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

    total$: Observable<number>;

    constructor(
      private cartService: CartService
    ) {
      this.total$ = this.cartService.cart$
      .pipe(
        map(products => products.length)
      );
    }
  
    ngOnInit() {
    }
  

}
