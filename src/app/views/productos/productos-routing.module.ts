import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListaProductosHomeComponent } from './lista-productos/lista-productos.component';

const routes: Routes = [
  {
    path: '',
    component: ListaProductosHomeComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductosRoutingModule { }
