import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductosRoutingModule } from './productos-routing.module';
import { SharedModule } from '../shared/shared.module';
import { ListaProductosHomeComponent } from './lista-productos/lista-productos.component';
import { MaterialModule } from '../../shared/material.module';
import { ProductoCardComponent } from './producto-card/producto-card.component';


@NgModule({
  declarations: [
    ListaProductosHomeComponent,
    ProductoCardComponent
  ],
  imports: [
    CommonModule,
    ProductosRoutingModule,
    SharedModule,
    MaterialModule
  ],
  exports:[
    ListaProductosHomeComponent,
    ProductoCardComponent
  ]
})
export class ProductosModule { }
