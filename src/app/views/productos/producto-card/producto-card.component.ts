import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CartService } from '../../../services/cart/cart.service';

@Component({
  selector: 'app-producto-card',
  templateUrl: './producto-card.component.html',
  styleUrls: ['./producto-card.component.scss']
})
export class ProductoCardComponent implements OnInit {
  @Input() product;
  @Output() productClicked: EventEmitter<any> = new EventEmitter();

  today = new Date();
  constructor( private cartService: CartService) { }

  ngOnInit(): void {
  }

  addCart() {
    console.log('añadir al carrito');
    this.cartService.addCart(this.product);
    // this.productClicked.emit(this.product.id);
  }

}
