import { Component, OnInit } from '@angular/core';
import { ProductoService } from '../../../services/producto/producto.service';

@Component({
  selector: 'app-lista-productos-home',
  templateUrl: './lista-productos.component.html',
  styleUrls: ['./lista-productos.component.scss']
})
export class ListaProductosHomeComponent implements OnInit {
  products: [] = [];
  constructor( private productsService: ProductoService) { }

  ngOnInit(): void {
    this.getProducts();
  }

  clickProduct(id: number) {
    console.log('product');
    console.log(id);
  }

  getProducts() {
    this.productsService.getAllProducts()
    .subscribe(data => {
      this.products = data.data;
    });
  }

  

}
