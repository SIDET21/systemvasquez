import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.scss']
})
export class BannerComponent implements OnInit {

  slides: string[] = [
    'assets/img/images/banner-1.jpg',
    'assets/img/images/banner-2.jpg',
    'assets/img/images/banner-3.jpg',
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
