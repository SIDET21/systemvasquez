import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { HomeRoutingModule } from "./home-routing.module";
import { HomeComponent } from "./home/home.component";
import { BannerComponent } from "./banner/banner.component";
import { SharedModule } from "../shared/shared.module";
import { ProductosModule } from "../productos/productos.module";
@NgModule({
  declarations: [HomeComponent, BannerComponent],
  imports: [CommonModule, HomeRoutingModule, SharedModule, ProductosModule],
})
export class HomeModule {}
