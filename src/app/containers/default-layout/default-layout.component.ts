import {Component} from '@angular/core';
import { SecurityService } from '../../utils/security/security.service';
import { navItems } from '../../_nav';

@Component({
  selector: 'app-dashboard',
  templateUrl: './default-layout.component.html'
})
export class DefaultLayoutComponent {
  public sidebarMinimized = false;
  public navItems = navItems;

  constructor(private seguridadService : SecurityService){

  }

  toggleMinimize(e) {
    this.sidebarMinimized = e;
  }

  cerrarSesion() {
    this.seguridadService.cerrarSesion();
  }
}
