

import { IconSetService } from '@coreui/icons-angular';
import { freeSet } from '@coreui/icons';
import { HeaderComponent } from './views/shared/header/header.component';

import { Component, OnInit, Inject, Renderer2, ElementRef, ViewChild } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/filter';
import { DOCUMENT } from '@angular/common';
import { LocationStrategy, PlatformLocation, Location } from '@angular/common';
@Component({
  // tslint:disable-next-line
  selector: 'body',
  template: '<router-outlet></router-outlet>',
  providers: [IconSetService],
})
export class AppComponent implements OnInit {
  constructor(
    private router: Router,
    public iconSet: IconSetService
  ) {
    // iconSet singleton
    iconSet.icons = { ...freeSet };
  }

  ngOnInit() {
    // this.router.events.subscribe((evt) => {
    //   if (!(evt instanceof NavigationEnd)) {
    //     return;
    //   }
    //   window.scrollTo(0, 0);
    // });
  }
  //private _router: Subscription;
 // @ViewChild(HeaderComponent) navbar: HeaderComponent;

//   constructor( private renderer : Renderer2, private router: Router, @Inject(DOCUMENT,) private document: any, private element : ElementRef, public location: Location) {}
//   ngOnInit() {
//     //   var navbar : HTMLElement = this.element.nativeElement.children[0].children[0];
//     //   this._router = this.router.events.filter(event => event instanceof NavigationEnd).subscribe((event: NavigationEnd) => {
//     //       if (window.outerWidth > 991) {
//     //           window.document.children[0].scrollTop = 0;
//     //       }else{
//     //           window.document.activeElement.scrollTop = 0;
//     //       }
//     //       this.navbar.sidebarClose();
//     //   });
//     //   this.renderer.listen('window', 'scroll', (event) => {
//     //       const number = window.scrollY;
//     //       if (number > 150 || window.pageYOffset > 150) {
//     //           // add logic
//     //           navbar.classList.remove('navbar-transparent');
//     //       } else {
//     //           // remove logic
//     //           navbar.classList.add('navbar-transparent');
//     //       }
//     //   });
//     //   var ua = window.navigator.userAgent;
//     //   var trident = ua.indexOf('Trident/');
//     //   if (trident > 0) {
//     //       // IE 11 => return version number
//     //       var rv = ua.indexOf('rv:');
//     //       var version = parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
//     //   }
//     //   if (version) {
//     //       var body = document.getElementsByTagName('body')[0];
//     //       body.classList.add('ie-background');

//     //   }

//   }

}
