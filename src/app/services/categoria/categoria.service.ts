import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { env } from 'process';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ApiResponseModel } from '../../models/api-response-model';
import { categoria } from '../../models/categoria/categoria';

const API_URL =env.URL_SERVICES;
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  })
};
@Injectable({
  providedIn: 'root'
})
export class CategoriaService {

  constructor(private http:HttpClient) { }

  getCategoria(): Observable<ApiResponseModel<categoria[]>> {
    return this.http.get<ApiResponseModel<categoria[]>>('assets/data/dataCategoria.json');
  }



}
