import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
// import { AngularFireStorage } from '@angular/fire/storage';
import {environment as env} from '../../../environments/environment'
import { ApiResponseModel } from '../../models/api-response-model';
import { producto } from '../../models/producto/producto';

const API_URL =env.URL_SERVICES;
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  })
};
@Injectable({
  providedIn: 'root'
})
export class ProductoService {

  constructor(private http:HttpClient,
     private storage: AngularFireStorage
     ) { }

  getProducts(): Observable<ApiResponseModel<producto[]>> {
    return this.http.get<ApiResponseModel<producto[]>>('assets/data/dataProduct.json');
  }


  getAll() {
    return this.http.get<Array<any>>(API_URL+"/product");
  }

  insertProducto(producto: producto): Observable<ApiResponseModel<any>> {
    const dataResponse : ApiResponseModel<any> = { success :true, message :"" ,data : {idproducto :Math.random().toString(36).substring(2)}

    }
    
    return of(dataResponse);
    // this.http.post<ApiResponseModel<any>>
    // (API_URL+ "", producto, httpOptions)
    // .pipe(map(resp => resp));

  }

  editProducto(producto: producto): Observable<ApiResponseModel<any>> {
    return this.http.put<ApiResponseModel<any>>
    (API_URL + "", producto, httpOptions)
    .pipe(map(resp => resp));
  }

  public tareaCloudStorage(nombreArchivo: string, datos: any) {
    return this.storage.upload(nombreArchivo, datos);
  }
  public referenciaCloudStorage(nombreArchivo: string) {
    return this.storage.ref(nombreArchivo);
  }


  /**HOME */


  getAllProducts() {
    //return this.http.get<Product[]>(`${environment.url_api}/products`);
    return this.http.get<ApiResponseModel<any>>('assets/data/listaproducts.json');
  }

}
