import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { env } from 'process';
import { Observable } from 'rxjs';
import { ApiResponseModel } from '../../models/api-response-model';
import { marca } from '../../models/marca/marca';

const API_URL =env.URL_SERVICES;
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  })
};
@Injectable({
  providedIn: 'root'
})
export class MarcaService {

  constructor(private http:HttpClient) { }

  getMarca(): Observable<ApiResponseModel<marca[]>> {
    return this.http.get<ApiResponseModel<marca[]>>('assets/data/dataMarca.json');
  }



}
