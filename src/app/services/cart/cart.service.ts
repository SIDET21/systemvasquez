import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { producto } from '../../models/producto/producto';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  private products: producto[] = [];
  private cart = new BehaviorSubject<producto[]>([]);
  cart$ = this.cart.asObservable();
  constructor() { }


  addCart(product: producto) {
    this.products = [...this.products, product];
    this.cart.next(this.products);
  }
}
