export class UserLogin {
  user: string;
  password: string;
}

export class UserSesionData {
  token: string;
}
