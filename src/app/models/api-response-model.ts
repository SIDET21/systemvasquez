export class ApiResponseModel<T> {
	success: boolean;
	message: string;
	validations?: any;
	data: T;
}