export class producto {
  producto_id?: number;
  nombre?: string;
  descripcion?: string;
  precio?: string;
  descuento?: string;
  stock?: number;
  marca_id?: number;
  subcategoria_id?: number;
  estado?: number;
}


export class DataFileUpload {
  
  listImagenes: string [];

}