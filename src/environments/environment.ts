// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  URL_SERVICES: "",
  firebase: {
    // apiKey: '<your-api-key>',
    // authDomain: '<your-auth-domain>',
    // databaseURL: '<your-database-url>',
    // projectId: '<your-project-id>',
    // storageBucket: '<your-project-id>.appspot.com',
    // messagingSenderId: '<your-messaging-sender-id>'
    apiKey: "AIzaSyBqtsmknujXsnSEAFbb-dkWatpSwdTNGAc",
    authDomain: "develop-imagenes.firebaseapp.com",
    projectId: "develop-imagenes",
    databaseURL: "https://blabla.firebaseio.com",
    storageBucket: "develop-imagenes.appspot.com",
    messagingSenderId: "437071609107",
    appId: "1:437071609107:web:2cb82dbac8afa57a80eee6",
  },
};
